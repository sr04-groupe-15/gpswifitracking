import React from 'react';
import Source from './src';
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import {appReducers}  from "./src/reducers";

const store = createStore(appReducers, applyMiddleware(thunk));
export default function App() {
    return(
        <Provider store={store}>
            <Source/>
        </Provider>
        )
}
