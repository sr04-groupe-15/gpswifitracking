import React, {useEffect} from 'react';
import {View} from "react-native"
import MapView, {Marker} from 'react-native-maps';
import {Ionicons} from '@expo/vector-icons';
import {useSelector, useDispatch} from "react-redux";

import styles from "./styles";
import {actFetchLocation} from "../../actions";

export default function MapDessin() {
    const coord = useSelector(state=>state.locations);
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(actFetchLocation);
        console.log(coord);
    },coord);
    return (
        <View style={styles.container}>
            <MapView style={styles.mapStyle}
                     region={{
                         latitude: coord.latitude,
                         longitude: coord.longitude,
                         latitudeDelta: 0.0041,
                         longitudeDelta: 0.0021 }}
            >
                <Marker coordinate={coord}>
                    <Ionicons name="md-radio-button-on"/>
                </Marker>
            </MapView>
        </View>
    );
}
