import React, {useState, useEffect} from 'react';
import {Text, View, NetInfo} from 'react-native';
import {useNetInfo} from "@react-native-community/netinfo";
import * as Network from 'expo-network';
import wifi from 'react-native-android-wifi';
const YourComponent = () => {
    const netInfo = useNetInfo();
    console.log(netInfo);
    return (
        <View>
            {/*<Text>Type: {netInfo.type}</Text>*/}
            {/*<Text>Is Connected? {netInfo.isConnected.toString()}</Text>*/}
        </View>
    );
};

export default function GetInfo () {


    Network.getNetworkStateAsync().then(state => console.log(state));
    Network.getIpAddressAsync().then(state => console.log("ip address: "+state));
    Network.getMacAddressAsync().then(state=>console.log("mac address: "+state));


    return (
        <YourComponent/>
    )
}
