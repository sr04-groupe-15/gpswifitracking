import * as types from "./../constants/ActionTypes";
const initialState = {
    latitude:0,
    longitude:0
};

export default (state = initialState, { type, payload }) => {
    console.log(state);
    switch (type) {
        case types.FETCH_LOCATIONS:
            return { ...state };
        case types.UPDATE_LOCATIONS:
            state = payload;
            return {...state};
        default:
            return state;
    }
};
