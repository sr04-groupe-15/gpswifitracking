import { combineReducers } from "redux";
import locations from "./locations";

export const appReducers = combineReducers({
    locations
});
