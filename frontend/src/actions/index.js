import * as types from "./../constants/ActionTypes";

export const actFetchLocation = () => dispatch => dispatch({
    type: types.FETCH_LOCATIONS
})
export const actUpdateLocation = ({latitude, longitude}) => dispatch => dispatch({
    type: types.UPDATE_LOCATIONS,
    payload: {latitude, longitude}
})
