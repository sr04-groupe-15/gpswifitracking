import React from 'react';
import {View} from "react-native"

import styles from "./styles";

import {MapDessin, FindMe, GetInfo} from "./components";


export default function Source() {
    return (
        <View style={styles.container}>
            {/*<FindMe/>*/}
            {/*<MapDessin/>*/}
            <GetInfo/>
        </View>
    );
}
